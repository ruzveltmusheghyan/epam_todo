import React from "react";
import { AddItem, Heading, TodoList, Search } from "..";
class App extends React.Component {
  state = {
    todos: localStorage.getItem("todos")
      ? JSON.parse(localStorage.getItem("todos"))
      : [],
  };

  componentDidUpdate() {
    localStorage.setItem("todos", JSON.stringify(this.state.todos));
  }

  render() {
    const { todos } = this.state;

    const handleUpdate = (todo) => {
      console.log(todo);
      if (Array.isArray(todo)) {
        this.setState({
          todos: todo,
        });
      } else {
        this.setState({
          todos: [...todos, todo],
        });
      }
    };

    return (
      <div className="app-container">
        <Heading />
        <AddItem todos={todos} handleUpdate={handleUpdate} />
        <Search />
        <TodoList todos={todos} handleUpdate={handleUpdate} />
      </div>
    );
  }
}

export default App;

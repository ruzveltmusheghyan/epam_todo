import React from "react";
import { Header } from "../../ui";
class TodoHeading extends React.Component {
  render() {
    return (
      <div className="container">
        <Header text="Todo app" Tag="h1" />
      </div>
    );
  }
}
export default TodoHeading;

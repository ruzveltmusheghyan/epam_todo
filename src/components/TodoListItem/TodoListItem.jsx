import classNames from "classnames";
import React from "react";
import { Button } from "../../ui/";
import styles from "./TodoListItem.module.css";

class TodoListItem extends React.Component {
  state = {
    important: this.props.todo.important,
    done: this.props.todo.done,
    inEditMode: false,
    value: this.props.todo.text,
  };

  render() {
    const { todo, handleImportant, handleDelete, handleEdit, handleComplete } =
      this.props;
    const { important, done, inEditMode, value } = this.state;

    const changeEdit = (id) => {
      this.setState({
        inEditMode: !inEditMode,
      });
      handleEdit(id, value);
    };
    const changeImportant = (id) => {
      this.setState({
        important: !important,
      });
      handleImportant(id);
    };
    const changeCompleted = (id) => {
      this.setState({
        done: !done,
      });
      handleComplete(id);
    };

    const {
      input: classInput,
      activeinput: classActive,
      done: classDone,
      important: classImportant,
      flex: classFlex,
    } = styles;

    return (
      <div className={styles.todo}>
        <div>
          <input
            className={classNames(classInput, {
              [classActive]: inEditMode,
              [classDone]: done,
              [classImportant]: important,
            })}
            type="text"
            value={value}
            onChange={(e) =>
              this.setState({
                value: e.target.value,
              })
            }
            readOnly={!inEditMode}
          />
        </div>
        <div className={classFlex}>
          <Button
            text={`${!inEditMode ? "Edit" : "Save"}`}
            onClick={() => changeEdit(todo.id)}
          />
          <Button
            text="important"
            active={important}
            onClick={() => changeImportant(todo.id)}
          />
          <Button
            text="done"
            active={done}
            onClick={() => changeCompleted(todo.id)}
          />
          <Button text="Delete" onClick={() => handleDelete(todo.id)} />
        </div>
      </div>
    );
  }
}

export default TodoListItem;

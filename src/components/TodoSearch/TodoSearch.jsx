import { Input, Button, Header } from "../../ui";
import React from "react";
class TodoSearch extends React.Component {
  render() {
    return (
      <div className="container">
        <Header Tag="h3" text="Search..." />
        <div className="flex">
          <Input placeholder="Search for todos..." />
          <Button text="All" />
          <Button text="important" />
          <Button text="Done" />
        </div>
      </div>
    );
  }
}

export default TodoSearch;

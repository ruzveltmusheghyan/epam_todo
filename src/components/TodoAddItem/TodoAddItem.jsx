import { Input, Button } from "../../ui";
import React from "react";
class TodoAddItem extends React.Component {
  state = {
    error: false,
    todo: "",
  };
  render() {
    const handleSubmit = () => {
      const { todo, error } = this.state;
      const { todos, handleUpdate } = this.props;

      if (todo.match(/^\s*$/))
        return this.setState({
          error: !error,
        });
      const newTodo = {
        id: todos.length + 1,
        text: todo,
        done: false,
        important: false,
      };

      handleUpdate(newTodo);
      this.setState({
        todo: "",
        error: false,
      });
    };

    return (
      <>
        {this.state.error ? (
          <span style={{ color: "red", fontSize: "1rem" }}>
            Please write a todo!
          </span>
        ) : null}
        <div className="flex">
          <Input
            value={this.state.todo}
            onChange={(e) =>
              this.setState({
                todo: e.target.value,
              })
            }
            placeholder="Add todo "
          />
          <Button text="Add to list" onClick={() => handleSubmit()} />
        </div>
      </>
    );
  }
}

export default TodoAddItem;

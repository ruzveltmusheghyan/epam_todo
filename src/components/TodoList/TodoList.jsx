import React from "react";
import { Header } from "../../ui";
import TodoListItem from "../TodoListItem/TodoListItem";
import styles from "./TodoList.module.css";

class TodoList extends React.Component {
  render() {
    const { handleUpdate, todos } = this.props;

    const IMPORTANT = "important";
    const DONE = "done";
    const TEXT = "text";

    const handleDelete = (id) => {
      const newTodos = todos.filter((todo) => todo.id !== id);
      handleUpdate(newTodos);
    };

    const handleChange = (prop, id, text) => {
      console.log(prop);
      const newTodos = todos.map((todo) => {
        if (todo.id === id) {
          return text
            ? { ...todo, [prop]: text }
            : { ...todo, [prop]: !todo[prop] };
        }
        return todo;
      });
      handleUpdate(newTodos);
    };

    const handleImportant = (id) => {
      handleChange(IMPORTANT, id);
    };
    const handleComplete = (id) => {
      handleChange(DONE, id);
    };
    const handleEdit = (id, value) => {
      handleChange(TEXT, id, value);
    };

    return (
      <div className={styles.card}>
        <Header text="My tasks" Tag="h3" />
        {todos.map((todo) => {
          return (
            <TodoListItem
              handleImportant={handleImportant}
              handleDelete={handleDelete}
              handleEdit={handleEdit}
              handleComplete={handleComplete}
              key={todo.id}
              todo={todo}
            />
          );
        })}
      </div>
    );
  }
}

export default TodoList;

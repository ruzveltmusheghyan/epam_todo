import classNames from "classnames";
import React from "react";
import styles from "./Button.module.css";
class Button extends React.Component {
  render() {
    const { active, onClick, text } = this.props;
    const { active: activeClass, button: classButton } = styles;
    return (
      <button
        className={classNames(classButton, { [activeClass]: active })}
        onClick={onClick}
      >
        {text}
      </button>
    );
  }
}

export default Button;

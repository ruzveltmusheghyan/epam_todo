import React from "react";
import styles from "./Input.module.css";
class Input extends React.Component {
  render() {
    const { value, onChange, placeholder } = this.props;
    return (
      <input
        value={value}
        onChange={onChange}
        className={styles.input}
        placeholder={placeholder}
        type="text"
      />
    );
  }
}

export default Input;

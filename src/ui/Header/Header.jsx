import styles from "./Header.module.css";
import React from "react";
class Header extends React.Component {
  render() {
    const { Tag, text } = this.props;

    return (
      <>
        <Tag>{text}</Tag>
        <div className={styles.line}></div>
      </>
    );
  }
}

export default Header;
